<?php

namespace App\Http\Controllers;
use App\model\docente;
use Illuminate\Http\Request;

class docentecontrol extends Controller
{
    public function index(){
        return docente::all();
    }
     public function store(Request $request){
        $docente =new docente();
        $docente->fill($request->toArray())->save();
        return $docente;
    }
    public function destroy($id_docente){
        $docente=docente::find($id_docente);
        $docente->delete();
    
    }
}
