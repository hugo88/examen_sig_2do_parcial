<?php

namespace App\Http\Controllers;
use App\model\alumno;
use Illuminate\Http\Request;

class alumnocontrol extends Controller
{
    public function index(){
        return alumno::all();
    }
     public function store(Request $request){
        $alumno =new alumno();
        $alumno->fill($request->toArray())->save();
        return $alumno;
    }
    public function destroy($id_alumno){
        $alumno=alumno::find($id_alumno);
        $alumno->delete();
    
    }
}
