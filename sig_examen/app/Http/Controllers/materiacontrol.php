<?php

namespace App\Http\Controllers;
use App\model\materia;
use Illuminate\Http\Request;

class materiacontrol extends Controller
{
    public function index(){
        return materia::all();
    }
     public function store(Request $request){
        $materia =new materia();
        $materia->fill($request->toArray())->save();
        return $materia;
    }
    public function destroy($id_materia){
        $materia=materia::find($id_materia);
        $materia->delete();
    
    }
}
