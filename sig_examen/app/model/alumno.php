<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class alumno extends Model
{
            protected $table='alumno';

    protected $primaryKey ='id_alumno';
    public $TIMESTAMPS=true;
    const created_at='create_ad';
    const update_at='update_ad';

    protected $fillable=[
        'nombre',
        'apellido',
        'email',
        'telefono',
        'direccion',
        'create_ad',
        'update_ad'
    ];
}
