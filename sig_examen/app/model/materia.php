<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class materia extends Model
{
    protected $table='materia';

    protected $primaryKey ='id_materia';
    public $TIMESTAMPS=true;
    const created_at='create_ad';
    const update_at='update_ad';

    protected $fillable=[
        'nombre',
        'horario',
        'id_profesor',
        'create_ad',
        'update_ad'
    ];
}
