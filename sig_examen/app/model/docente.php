<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class docente extends Model
{
               protected $table='docente';

    protected $primaryKey ='id_docente';
    public $TIMESTAMPS=true;
    const created_at='create_ad';
    const update_at='update_ad';

    protected $fillable=[
        'nombre',
        'apellido',
        'email',
        'telefono',
        'materia',
        'create_ad',
        'update_ad'
    ];
}
