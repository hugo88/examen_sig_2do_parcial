<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class notas extends Model
{
    protected $table='notas';

    protected $primaryKey ='id_notas';
    public $TIMESTAMPS=true;
    const created_at='create_ad';
    const update_at='update_ad';

    protected $fillable=[
        'primerparcial',
        'segundoparcial',
        'final',
        'nota_final',
        'id_alumno',
        'id_materia',
        'id_docente',
        'create_ad',
        'update_ad'
    ];
}
