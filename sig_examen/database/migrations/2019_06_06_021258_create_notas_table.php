<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notas', function (Blueprint $table) {
            $table->bigIncrements('id_notas');
            $table->integer('primerparcial')->nullable();
            $table->integer('segundoparcial')->nullable();
            $table->integer('final')->nullable();
            $table->decimal('nota_final')->nullable();
            $table->integer('id_alumno')->nullable();
            $table->integer('id_materia')->nullable();
            $table->integer('id_docente')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notas');
    }
}
